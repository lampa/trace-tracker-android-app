package kiv_zcu_cz_TraceTRacker;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;








import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.xmlpull.v1.XmlSerializer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Environment;
import android.util.Log;
import android.util.Xml;
/***********************************************************************************************************************
*
* This file is part of the ${PROJECT_NAME} project

* ==========================================
*
* Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
*
***********************************************************************************************************************
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
* the License. You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
* an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
* specific language governing permissions and limitations under the License.
*
***********************************************************************************************************************
*
* ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
*
**********************************************************************************************************************/
public class ExportSave extends Activity {

	
	
	private final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			+ "abcdefghijklmnopqrstuv"; 
	private Random rnd = new Random();
	private String TAG="exp";
	private Aproximace apr = new Aproximace(2);
	
	
	public void saveXML(int kroky, double delka, double prumerychlost, int delkahod
			,int delkamin,int delkasec,List<Double> listvzdalenosti,GregorianCalendar konec
			,GregorianCalendar zacatek,List<Double>zemsirka,List<Double>zemdelka,String koment)
	{
		
		// all xml files are saving to the folder "TraceTRackerData"
		File root = new File(Environment.getExternalStorageDirectory(), "TraceTRackerData");
        if (!root.exists()) {
            root.mkdirs();
        }
	    //create hash name  		
		StringBuilder sb = new StringBuilder(8); 
		for( int i = 0; i < 8; i++ ) {
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		}
		String filename = sb.toString() + ".xml";

		File newxmlfile = new File(root,filename);
			
  
		        FileOutputStream fileos = null;
		        try{
		            fileos = new FileOutputStream(newxmlfile);

		        }catch(FileNotFoundException e)
		        {
		            Log.e("FileNotFoundException",e.toString());
		        }
		        //Create xml 
		        XmlSerializer xml = Xml.newSerializer();
		        try{
		        	xml.setOutput(fileos, "UTF-8");
		        	
		        	xml.startDocument(null, Boolean.valueOf(true));
		        	xml.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
		        	
		        	xml.startTag(null,"xml");
		        	xml.startTag(null, "head");
		        	
		        	 xml.startTag(null, "hash");
					  xml.text(sb.toString());
					  xml.endTag(null, "hash");
		        	
				 xml.startTag(null, "start");
				 
				 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); // Set your date format
				 String currentData = sdf.format(zacatek.getTime());
				  xml.text(currentData);
				  xml.endTag(null, "start");
				  
				  
				  xml.startTag(null, "steps");
				  xml.text(String.valueOf(kroky));
				  xml.endTag(null, "steps");
				  
				 xml.startTag(null, "distance");
				  xml.text(String.format("%.3f",delka));
				  xml.endTag(null, "distance");
				  
				
				
				  xml.startTag(null, "time");
				  xml.text(String.valueOf(delkahod) + ":"+String.valueOf(delkamin)
						  +":"+String.valueOf(delkasec));
				  xml.endTag(null, "time");
				 
				  xml.startTag(null, "comment");
				  xml.text(koment);
				  xml.endTag(null, "comment");
				  
				
				  
				  xml.endTag(null, "head");
				  
				  xml.startTag(null, "data");
				  for(int i = 0;i<listvzdalenosti.size();i++)
					  xml.text(String.format("%.3f;", apr.aproximovat(listvzdalenosti.get(i))));
				  xml.endTag(null, "data");
				  
				  if(zemsirka.size()!=0){
				  xml.startTag(null, "coordinate");
				  for(int i = 0;i<zemsirka.size();i++)
					  xml.text(String.valueOf(zemsirka.get(i)+":"+zemdelka.get(i)+";"));
				  xml.endTag(null, "coordinate");

				  }
				  
				  xml.endTag(null,"xml");
				  xml.endDocument();
				  
				  xml.flush();
				 
		        fileos.close();
		        

		        AlertDialog ad = new AlertDialog.Builder(this).create();
		        ad.setCancelable(false); // This blocks the 'BACK' button
		        ad.setMessage("Data saved!");
		        ad.setButton("OK", new DialogInterface.OnClickListener() {
		        	@Override
		        	public void onClick(DialogInterface dialog, int which) {
		        		dialog.dismiss();                    
		        	}
		        });
		        ad.show();
		       
		        }catch(Exception e)
		        {
		            Log.e("Exception","Exception occured in wroting");
		        }
	    	
		
	}

	public void export(String username, String password)
	{
		String url = "http://tracetracker.truework.cz/send/?username=" + username +"&password="
				+ hashPassword(password); 
	
		
		File root = new File(Environment.getExternalStorageDirectory(), "TraceTRackerData");
        if (!root.exists()) {
            return;
        }
        //read all files from folder
        File file[] = root.listFiles();
        Log.d("Filess", "Size: "+ file.length);
        for (int i=0; i < file.length; i++)
        {  

        		 // Create a new HttpClient and Post Header  
        	try { 
        		FileInputStream fis = new FileInputStream(file[i]);
        	    byte[] data = new byte[(int) file[i].length()];
        	    fis.read(data);
        	    fis.close();
        	    //
        	    String myXML = new String(data, "UTF-8");
        		
        		HttpClient httpClient = new DefaultHttpClient(); 
        		HttpContext localContext = new BasicHttpContext(); 
        		HttpPost httpPost = new HttpPost(url); 

        		List<NameValuePair> dataToPost = new ArrayList<NameValuePair>(); 
        		
        		dataToPost.add(new BasicNameValuePair("xml", myXML)); 

        		httpPost.setEntity(new UrlEncodedFormEntity(dataToPost)); 

				HttpResponse response = httpClient.execute(httpPost, localContext); 
        		
        		} catch (Exception e) { 
        		e.printStackTrace(); 
        		}

				Log.d(TAG, "executed");

        	
	
        }
	}
	/**
	 * Method for hash password
	 * @param pass - user password
	 * @return hash password
	 */
	public String hashPassword(String pass) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hash = md.digest(pass.getBytes("UTF-8")); 
			StringBuffer sb = new StringBuffer();
			for(int i = 0; i < hash.length; i++)
			{ 
				sb.append(Integer.toHexString((hash[i] & 0xFF) | 0x100).substring(1,3));
			} 
			return sb.toString(); 
		} 
		catch (NoSuchAlgorithmException e) 
		{ // TODO Auto-generated catch block e.printStackTrace(); 
			
		} 
		catch (UnsupportedEncodingException e) { 
			// TODO Auto-generated catch 
			
		} 
		return pass; 
		}

}

