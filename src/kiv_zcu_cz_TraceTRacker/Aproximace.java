package kiv_zcu_cz_TraceTRacker;
/***********************************************************************************************************************
*
* This file is part of the ${PROJECT_NAME} project

* ==========================================
*
* Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
*
***********************************************************************************************************************
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
* the License. You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
* an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
* specific language governing permissions and limitations under the License.
*
***********************************************************************************************************************
*
* ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
*
**********************************************************************************************************************/
public class Aproximace {

	private static final int defaultN = 2;
	private double[] pole;
	private int stred;
	
	public Aproximace() {
		this(defaultN);
	}
	
	public Aproximace(int n) {
		pole = new double[2*n + 1];
		stred = n;
	}
	
	public double aproximovat(double aproximovany) {
		posun();
		pole[pole.length - 1] = aproximovany;
		aproximuj();
		return pole[stred];
	}
	
	private void posun() {
		for(int i = 0; i < pole.length - 1; i++) {
			this.pole[i] = this.pole[i + 1];
		}
	}
	
	private void aproximuj() {
		double suma = 0.0;
		int pocet = 0;
		
		for(int i = 0; i < 1; i++) {
			for(int j = i; j < pole.length - i; j++) {
				suma += pole[j];
				pocet++;
			}
		}
		
		pole[stred] = suma / pocet;
	}
	
}
