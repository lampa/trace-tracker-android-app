package kiv_zcu_cz_TraceTRacker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.example.tracetracker.R;
import com.example.tracetracker.R.layout;
import com.example.tracetracker.R.menu;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/***********************************************************************************************************************
*
* This file is part of the ${PROJECT_NAME} project

* ==========================================
*
* Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
*
***********************************************************************************************************************
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
* the License. You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
* an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
* specific language governing permissions and limitations under the License.
*
***********************************************************************************************************************
*
* ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
*
**********************************************************************************************************************/
public class ExportActivity extends Activity {

	
	Button export;
	Button back;
	EditText user;
	EditText pass;
	String udaj;
	double vyska;
	ExportSave expsave = new ExportSave();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_export);
		export = (Button)findViewById(R.id.Exp);
		user = (EditText)findViewById(R.id.editText1);
		pass = (EditText)findViewById(R.id.editText2);
		
		back = (Button)findViewById(R.id.buttonback);
		vyska = getIntent().getExtras().getDouble("vyska");
		
		udaj = readFromFile();
		
		if(udaj.equals(""))
		{
			user.setText("");
			pass.setText("");
		}
		else
		{
			 String[] items = udaj.split(";");
			 user.setText(items[0]);
			 pass.setText(items[1]);
			
		}
		/*
		 * Button for export xml files to databases
		 * Read username and password and after call method for export
		 * After exporting save data about user
		 */
		export.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String username = user.getText().toString();
				String password = pass.getText().toString();
				
				Log.i("nevim", username + ": " + password+ ": ");
			
				File root = new File(Environment.getExternalStorageDirectory(), "TraceTRackerData");
		        if (!root.exists()) {
		            return;
		        }
		        
		        File file[] = root.listFiles();
		     
		        
		        String text = "";
		       
		        
		        
		        for (int i=0; i < file.length; i++)
		        { 
		        	text +=file[i].getName()+";";
		        }
	
				expsave.export(username, password);
				
				writeToFile((username+";"+password+";"+vyska+";" + text));
				
				finish();
			}
			
			
		});
		
		back.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 
				 finish();
				    
			}
			
			
		});
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.export, menu);
		return true;
	}
	
	private void writeToFile(String data) {
	    try {
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("config.txt", Context.MODE_PRIVATE));
	        outputStreamWriter.write(data);
	        outputStreamWriter.close();
	    }
	    catch (IOException e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } 
	}


	private String readFromFile() {

	    String ret = "";

	    try {
	        InputStream inputStream = openFileInput("config.txt");

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	            }

	            inputStream.close();
	            ret = stringBuilder.toString();
	        }
	    }
	    catch (FileNotFoundException e) {
	        Log.e("login activity", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("login activity", "Can not read file: " + e.toString());
	    }

	    return ret;
	}

}
