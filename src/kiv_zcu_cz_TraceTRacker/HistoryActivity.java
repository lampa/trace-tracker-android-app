package kiv_zcu_cz_TraceTRacker;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.example.tracetracker.R;
import com.example.tracetracker.R.layout;
import com.example.tracetracker.R.menu;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.Html;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class HistoryActivity extends Activity {

	
	Button back;
	Button deleteexp;
	Button deleteall;
	TextView txt;
	AlertDialog.Builder alert;

	ArrayList<File> exp = new ArrayList<File>();
	ArrayList<File> vse = new ArrayList<File>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_history);
		back = (Button)findViewById(R.id.buttonBacki);
		txt = (TextView)findViewById(R.id.txthis);
		deleteexp = (Button)findViewById(R.id.buttondeleteexp);
		deleteall = (Button)findViewById(R.id.butdeleteall);
		
		
		txt.setMovementMethod(ScrollingMovementMethod.getInstance());
		
		deleteexp.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub 
				 callDialog("Do you want delete exported files?",true);    
			}
			
			
		});
		
		deleteall.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub 
				 callDialog("Do you want delete all files?",false);    
			}
			
			
		});
		
		back.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub 
				 finish();	    
			}
			
			
		});
		
		showData();
	}

	
	private void showData()
	{
		File root = new File(Environment.getExternalStorageDirectory(), "TraceTRackerData");
        if (!root.exists()) {
        	txt.setText("No save data");
            return;
        }
        //Read all files from folder
        File file[] = root.listFiles();
        if(file.length == 0)
        {
        	txt.setText("No save data");
            return;
        	
        }
        //create html string about file
        String text = "<p><b>Files:<b></p>";
       
        String[] savefile = readFromFile().split(";");
        
        for (int i=0; i < file.length; i++)
        { 
        	 
             Date time = new Date( file[i].lastModified());
             Calendar calendar = new GregorianCalendar();
             calendar.setTime(time);
             //Determine if the file was exported
             boolean bylexp = false;
             for(int k = 3;k<savefile.length;k++)
             {
            	 if(file[i].getName().equals(savefile[k]))
            	 { 
            		 bylexp = true;break;
            	 }
            	 
            	
             }
             //sets green or red color after exporting
             if(bylexp)
            	 {
            	 text +="<p><font color='green'>"+calendar.get(Calendar.DAY_OF_MONTH)
      			+":"+calendar.get(Calendar.MONTH)+":"+calendar.get(Calendar.YEAR)+"-"
            			 +file[i].getName()+"</font></p>";
            	 exp.add(file[i]);
            	 }
             else
            	 {text +="<p><font color='red'>"+
             calendar.get(Calendar.DAY_OF_MONTH)
      			+":"+calendar.get(Calendar.MONTH)+":"
             +calendar.get(Calendar.YEAR)+"-"+file[i].getName()+"</font></p>";
            	 
            	 }
             
        	vse.add(file[i]);
        }
		
        txt.setText(Html.fromHtml(text));
      
     
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.history, menu);
		return true;
	}

	
	private String readFromFile() {

	    String ret = "";

	    try {
	        InputStream inputStream = openFileInput("config.txt");

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	            }

	            inputStream.close();
	            ret = stringBuilder.toString();
	        }
	    }
	    catch (FileNotFoundException e) {
	        Log.e("login activity", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("login activity", "Can not read file: " + e.toString());
	    }

	    return ret;
	}
	
	
	/**
	 * This metod show dialog for write comments about running and save data
	 */
	public void callDialog(String ot,final boolean onlyexp)
	{
		  alert = new AlertDialog.Builder(this);
	        alert.setTitle("Really?");
			alert.setMessage(ot);

					alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				
			  if(onlyexp)
			  {
				  for(File soubor: exp)
				  {
					  File file = soubor;
					  
					  boolean deleted = file.delete();
					  
					    if (!deleted)
					      throw new IllegalArgumentException("Delete: deletion failed");
				  }
			  }
			  else{
				  for(File soubor: vse)
				  {
					  File file = soubor;
					  boolean deleted = file.delete();
					  if (!deleted)
					      throw new IllegalArgumentException("Delete: deletion failed");
				  }
			  }
				
				finish();
				startActivity(getIntent());
			  }
			});
			
				alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				
				
			  

			  }
			});
			alert.show();
		
	}
}
