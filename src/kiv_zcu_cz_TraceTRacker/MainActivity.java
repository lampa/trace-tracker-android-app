package kiv_zcu_cz_TraceTRacker;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;









import java.util.concurrent.TimeUnit;

import org.ambientdynamix.api.application.ContextEvent;
import org.ambientdynamix.api.application.ContextPluginInformation;
import org.ambientdynamix.api.application.ContextSupportInfo;
import org.ambientdynamix.api.application.ContextSupportResult;
import org.ambientdynamix.api.application.IContextInfo;
import org.ambientdynamix.api.application.IDynamixFacade;
import org.ambientdynamix.api.application.IDynamixListener;
import org.ambientdynamix.contextplugins.location.ILocationContextInfo;
import org.ambientdynamix.contextplugins.pedometer.IPedometerStepInfo;









import com.example.tracetracker.R;










import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.text.Editable;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

/***********************************************************************************************************************
*
* This file is part of the ${PROJECT_NAME} project

* ==========================================
*
* Copyright (C) ${YEAR} by University of West Bohemia (http://www.zcu.cz/en/)
*
***********************************************************************************************************************
*
* Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with
* the License. You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on
* an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
* specific language governing permissions and limitations under the License.
*
***********************************************************************************************************************
*
* ${NAME}, ${YEAR}/${MONTH}/${DAY} ${HOUR}:${MINUTE} ${USER}
*
**********************************************************************************************************************/ 
public class MainActivity extends Activity {

	private TextView txt;
	private TextView txtdis;
	private TextView txtspeed;
	private TextView txtstart;
	private TextView txtfinish;
	private TextView txttime;

	private String TAG = "nevim";
	
	private Button tg;
	private Button start;
	private Button export;
	private Button save;
	private Button restart;
	private Button history;
	
	private EditText txtvel;
	
	private RadioButton vkalhotach;
	private RadioButton vruce;
	
	private int kroky = 0;
	private boolean pripojeno = false; 
	private double DELKA_KROKU = 0.75; 
	private double DELKA_BEHU = 1.80;
	private double distance = 0.0;
	
 	private   IDynamixFacade dynamix;
 
	private int delkahod = 0;
	private int delkamin = 0;
	private int delkasec = 0;
	private double speed = 0;
	double vyska = 175;
	
	GregorianCalendar zacatek;
	GregorianCalendar konec;
	private List<Double> listvzdalenosti = new ArrayList<Double>();
	int predchopomsec;
	ExportSave expsave = new ExportSave();
	private List<Double> zemsirka = new ArrayList<Double>();
	private List<Double> zemdelka = new ArrayList<Double>();
	long pomcas;
	AlertDialog.Builder alert;
	private boolean stop = false;
	String koment = "";
	private boolean nastaven = false;
	
	
	private int khod;
	private int kmin;
	private int ksec;
	
	private double pomdist = 0;
	int sec;
	/**
	 *The first method, which is called when the application starts. Perform all settings.
	 */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
    
        setContentView(R.layout.activity_main);
        Log.i(TAG, "A1 - Program Start");
        //Setting TextView from layout.
        txt = (TextView)findViewById(R.id.txt1);
        txtdis = (TextView)findViewById(R.id.txt2);
        txtspeed = (TextView)findViewById(R.id.txtspeed);
        txtstart = (TextView)findViewById(R.id.textstart);
        txtfinish = (TextView)findViewById(R.id.textfinish);
        txttime = (TextView)findViewById(R.id.txttime);
        //Setting Button from layout.
        tg = (Button)findViewById(R.id.butBack);   
        export = (Button)findViewById(R.id.butexport);
        restart = (Button)findViewById(R.id.butRes);
        start = (Button)findViewById(R.id.start);
        save = (Button)findViewById(R.id.butsave);
        history = (Button)findViewById(R.id.buthist);
        
        save.setEnabled(false);
        
       
        
        txtvel = (EditText)findViewById(R.id.editText1);
        
        //Reading the last selected human height.
        //If not chosen, so the stock of default set 175cm.
        String soubor = readFromFile();
        
        if(soubor.equals(""))
        	txtvel.setText("175");
        else
        {
        	txtvel.setText(soubor);
        	
        }
        
        vkalhotach = (RadioButton)findViewById(R.id.radioKaspa);
        vkalhotach.setChecked(true);
      
        vruce = (RadioButton)findViewById(R.id.radioRuka);
        vruce.setChecked(false);
     
        start.setText("Connect");
       
        //The thread that runs in the background and every half second will refresh the screen.
        Thread t = new Thread() {

        	  @Override
        	  public void run() {
        	    try {
        	      while (!isInterrupted()) {
        	        Thread.sleep(500);
        	        runOnUiThread(new Runnable() {
        	          @Override
        	          public void run() {
        	            // update TextView
        	        	  if(stop == false){
        	        		  sec+=500;
        	        		  
        	        		//Every two second save the distance, which was over two seconds measured  
        					if (sec>=2000) {
								listvzdalenosti.add((distance - pomdist));
								pomdist = distance;
								sec=0;
							}
							txt.setText("Number of steps: " + kroky);
        	        	
      						txtdis.setText(String.format("Distance: %.3f m", distance));
      						
      						
      						txttime.setText("Time Running: " + delkahod
      								+ " : " +delkamin+ " : " + delkasec);
      					
      						txtfinish.setText("Finish time: " + khod + " : " + kmin + " : " + ksec);
      						txtspeed.setText(String.format("The avarage speed: %.3f m/s", speed));
      						
      						txt.invalidate();
      				    	txtdis.invalidate();
      				    	txttime.invalidate();
      				    	txtfinish.invalidate();
      				    	txtspeed.invalidate();
      				    	
        	        	  }
        	          }
        	        });
        	      }
        	    } catch (InterruptedException e) {
        	    }
        	  }
        	};

        	t.start();
        
        //Button exit aplication
        tg.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//save height to file
				writeToFile(String.valueOf(vyska));
				System.exit(0);
			}
        	
        	
        });
      
        
      //Button for restart aplication  
      restart.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				writeToFile(String.valueOf(vyska));
				finish();
				startActivity(getIntent());
			}
      	
      	
      });
      
      //First the button sets dynamix and next clicks starts the measurement/ stop
      start.setOnClickListener(new OnClickListener(){
      	
      	@Override
			public void onClick(View v) {
      		
      		if(pripojeno == false){
      		if (dynamix == null) {
				/*
				 * Bind to the Dynamix service using the Activity's 'bindService' method, which completes
				 * asynchronously. As such, you must wait until the 'onServiceConnected' method of the
				 * ServiceConnection 'sConnection' implementation is called (see below) before calling Dynamix
				 * methods.
				 */
      			
				bindService(new Intent(IDynamixFacade.class.getName()), sConnection, Context.BIND_AUTO_CREATE);
				Log.i(TAG, "A1 - Connecting to Dynamix...");
				stop = true;
				start.setText("Start");
				 pripojeno = true;
				
			} else {
				try {
					if (!dynamix.isSessionOpen()) {
						Log.i(TAG, "Dynamix connected... trying to open session");
						dynamix.openSession();
					} else
						Log.i(TAG, "Session is already open");
				} catch (RemoteException e) {
					Log.e(TAG, e.toString());
				}
			}
      		
      		}else {
				if (dynamix != null) {
					try {
						if(nastaven == false)
						{
							
							
							
							try {
								vyska = Integer.parseInt(txtvel.getText().toString());
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
								DELKA_KROKU = (vyska*0.42)/100;
								DELKA_BEHU  = (vyska*1.05)/100;
							
							
							//Sets start time of runnign
							zacatek = new GregorianCalendar();
							
							zacatek.setTime(new Date(System.currentTimeMillis()));
							pomcas = System.currentTimeMillis();
							Log.i(TAG,String.valueOf(pomcas));
							dynamix.resendAllCachedContextEvents(dynamixCallback);
							dynamix.openContextPluginConfigurationView(dynamixCallback,
									"org.ambientdynamix.contextplugins.sampleplugin");
							dynamix.contextRequest(dynamixCallback,"org.ambientdynamix.contextplugins.location", 
									"org.ambientdynamix.contextplugins.location");
							dynamix.contextRequest(dynamixCallback, "org.ambientdynamix.contextplugins.pedometer",
					"org.ambientdynamix.contextplugins.pedometer");
						
							int zhod = zacatek.get(Calendar.HOUR);
							int zmin = zacatek.get(Calendar.MINUTE);
							int zsec = zacatek.get(Calendar.SECOND);
							
							txtstart.setText("Start time: " + zhod + " : " + zmin + " : " + zsec);
							nastaven = true;
							start.setText("Stop");
							stop = false;
						}
						else{
						
							if(stop==false){
									
									GregorianCalendar calendar = new GregorianCalendar();
									
									calendar.setTime(new Date(System.currentTimeMillis()));
								
									//Sets finish time of running
									 khod = calendar.get(Calendar.HOUR);
									 kmin = calendar.get(Calendar.MINUTE);
									 ksec = calendar.get(Calendar.SECOND);

									 konec = calendar;	
									
								     long roz = konec.getTime().getTime() - zacatek.getTime().getTime();
 
								     delkasec = (int)roz/1000;
								     delkamin =  delkasec/60;
								     delkahod = delkamin/60;
									
								     delkasec -=delkamin*60;
								     delkamin -= delkahod*60;
								     
									
									 save.setEnabled(true);
									//Count avarge speed from running
									double rychlost = 0;
									if(distance > 0)
									{ 
										double pom = distance/((delkahod * 3600) + (delkamin*60) + delkasec);
										rychlost = pom;
									}
									
								
									speed = rychlost;
									
									start.setText("Start");
									stop = true;
									
							}
								else
								{
									
									
									start.setText("Stop");
									stop = false;
							
								}
						}
				
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						
						e.printStackTrace();
					}
					
					
				}else Log.i(TAG,"Dynamix is not connected");
      		}
      		
      	}
      });
      
      //Button for save running data to xml file
      save.setOnClickListener(new OnClickListener(){

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
	
			callDialog();
	
		}
    	  
    	  
      });
      //Button for export data to databases 
      export.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				 Bundle bud = new Bundle();
				 bud.putDouble("vyska", vyska);
				 Intent intent = new Intent(MainActivity.this, ExportActivity.class);
				 intent.putExtras(bud);
				    startActivity(intent);
				
			}
        	
        	
        });
      //Button for switch to History activity
      history.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				 Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
				    startActivity(intent);
				
			}
      	
      	
      });
        
    }
   
    
    
    public void onStart()
    {
    	super.onStart();
    	

    }
  
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    private ServiceConnection  sConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            // We're connected, so add ourselves as a Dynamix listener
        	Log.i(TAG, "A1 - Jsem tu"
        			+ "!");
        	try {
				Log.i(TAG, "A1 - Dynamix is connected!");
				// Create a Dynamix Facade using the incoming IBinder
				dynamix = IDynamixFacade.Stub.asInterface(service);
				// Create a Dynamix listener using the callback
				dynamix.addDynamixListener(dynamixCallback);
			} catch (Exception e) {
				Log.w(TAG, e);
			}
		}

		/**
		 * Indicates that a previously connected IDynamixFacade has been disconnected from Dynamix. This typically means
		 * that Dynamix has crashed or been shut down by Android to conserve resources. In this case,
		 * 'onServiceConnected' will be called again automatically once Dynamix boots again.
		 */
		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.i(TAG, "A1 - Dynamix is disconnected!");
			dynamix = null;
		}
    
    };
    
    /**
	 * Implementation of the IDynamixListener interface. For details on the IDynamixListener interface, see the Dynamix
	 * developer website.
	 */
	private IDynamixListener dynamixCallback = new IDynamixListener.Stub() {
		@Override
		public void onDynamixListenerAdded(String listenerId) throws RemoteException {
			Log.i(TAG, "A1 - onDynamixListenerAdded for listenerId: " + listenerId);
			// Open a Dynamix Session if it's not already opened
			if (dynamix != null) {
				if (!dynamix.isSessionOpen())
					dynamix.openSession();
				else
					registerForContextTypes();
			} else
				Log.i(TAG, "dynamix already connected");
		}

		@Override
		public void onDynamixListenerRemoved() throws RemoteException {
			Log.i(TAG, "A1 - onDynamixListenerRemoved");
		}

		@Override
		public void onSessionOpened(String sessionId) throws RemoteException {
			Log.i(TAG, "A1 - onSessionOpened");
			registerForContextTypes();
		}

		@Override
		public void onSessionClosed() throws RemoteException {
			Log.i(TAG, "A1 - onSessionClosed");
			
		}

		@Override
		public void onAwaitingSecurityAuthorization() throws RemoteException {
			Log.i(TAG, "A1 - onAwaitingSecurityAuthorization");
		}

		@Override
		public void onSecurityAuthorizationGranted() throws RemoteException {
			Log.i(TAG, "A1 - onSecurityAuthorizationGranted");
			registerForContextTypes();
		}

		@Override
		public void onSecurityAuthorizationRevoked() throws RemoteException {
			Log.w(TAG, "A1 - onSecurityAuthorizationRevoked");
		}

		@Override
		public void onContextSupportAdded(ContextSupportInfo supportInfo) throws RemoteException {
			Log.i(TAG, "A1 - onContextSupportAdded for " + supportInfo.getContextType() + " using plugin "
					+ supportInfo.getPlugin() + " | id was: " + supportInfo.getSupportId());
		}

		@Override
		public void onContextSupportRemoved(ContextSupportInfo supportInfo) throws RemoteException {
			Log.i(TAG, "A1 - onContextSupportRemoved for " + supportInfo.getSupportId());
		}

		@Override
		public void onContextTypeNotSupported(String contextType) throws RemoteException {
			Log.i(TAG, "A1 - onContextTypeNotSupported for " + contextType);
		}

		@Override
		public void onInstallingContextSupport(ContextPluginInformation plug, String contextType)
				throws RemoteException {
			Log.i(TAG, "A1 - onInstallingContextSupport: plugin = " + plug + " | Context Type = " + contextType);
		}

		@Override
		public void onContextPluginInstallProgress(ContextPluginInformation plug, int percentComplete)
				throws RemoteException {
			Log.i(TAG, "A1 - onContextPluginInstallProgress for " + plug + " with % " + percentComplete);
		}

		@Override
		public void onInstallingContextPlugin(ContextPluginInformation plug) throws RemoteException {
			Log.i(TAG, "A1 - onInstallingContextPlugin: plugin = " + plug);
		}

		@Override
		public void onContextPluginInstalled(ContextPluginInformation plug) throws RemoteException {
			Log.i(TAG, "A1 - onContextPluginInstalled for " + plug);
			/*
			 * Automatically create context support for specific context types
			 */
			for (String type : plug.getSupportedContextTypes()) {
				dynamix.addContextSupport(dynamixCallback, type);
			}
		}

		@Override
		public void onContextPluginUninstalled(ContextPluginInformation plug) throws RemoteException {
			Log.i(TAG, "A1 - onContextPluginUninstalled for " + plug);
		}

		@Override
		public void onContextPluginInstallFailed(ContextPluginInformation plug, String message) throws RemoteException {
			Log.i(TAG, "A1 - onContextPluginInstallFailed for " + plug + " with message: " + message);
		}

		@Override
		public void onContextEvent(ContextEvent event) throws RemoteException {
			
			if(stop==false){
			/*
			 * Log some information about the incoming event
			 */
			Log.i(TAG, "A1 - onContextEvent received from plugin: " + event.getEventSource());
			Log.i(TAG, "A1 - -------------------");
			Log.i(TAG, "A1 - Event context type: " + event.getContextType());
			Log.i(TAG, "A1 - Event timestamp " + event.getTimeStamp().toLocaleString());
			if (event.expires())
				Log.i(TAG, "A1 - Event expires at " + event.getExpireTime().toLocaleString());
			else
				Log.i(TAG, "A1 - Event does not expire");
			/*
			 * To illustrate how string-based context representations are accessed, we log each contained in the event.
			 */
			for (String format : event.getStringRepresentationFormats()) {
				Log.i(TAG,
						"Event string-based format: " + format + " contained data: "
								+ event.getStringRepresentation(format));
			}
			// Check for native IContextInfo
			if (event.hasIContextInfo()) {
				Log.i(TAG, "A1 - Event contains native IContextInfo: " + event.getIContextInfo());
				IContextInfo nativeInfo = event.getIContextInfo();
				
				Log.i(TAG, "A1 - IContextInfo implimentation class: " + nativeInfo.getImplementingClassname());
				//Example of using Location
				 if (nativeInfo instanceof ILocationContextInfo  ) {
			          ILocationContextInfo data = (ILocationContextInfo) nativeInfo;
			          Log.i(TAG, "Received ILocationContextInfo with location: " + data.getLatitude() + ":" + data.getLongitude());
			          zemsirka.add(data.getLatitude());
			          zemdelka.add(data.getLongitude());
			      }
				// Example of using IPedometerStepInfo
				if (nativeInfo instanceof IPedometerStepInfo) {
					IPedometerStepInfo stepInfo = (IPedometerStepInfo) nativeInfo;
					Log.i(TAG, "A1 - Received IPedometerStepInfo with RmsStepForce: " + stepInfo.getRmsStepForce());
					GregorianCalendar calendar = new GregorianCalendar();
					
					calendar.setTime(new Date(System.currentTimeMillis()));
				
					
					//After StepForce determine whether it's running or walking
					if(vkalhotach.isChecked() && vruce.isChecked() == false){
					if(stepInfo.getRmsStepForce() > 6.8)
					{
						kroky +=2;
						Log.i(TAG,"zaznamenal jsem beh v kapse");
						distance += DELKA_BEHU*2;
		
					}
					else if(stepInfo.getRmsStepForce()>0.6)
						{
						Log.i(TAG,"zaznamenal jsem chuzy v kapse");
							kroky ++;
							distance += DELKA_KROKU;
					
						}
					}else{
						if(stepInfo.getRmsStepForce() > 5.8)
						{
							kroky +=2;
							Log.i(TAG,"zaznamenal jsem beh v ruce");
							distance += DELKA_BEHU*2;
	
						}
						else if(stepInfo.getRmsStepForce()>0.6)
							{
							Log.i(TAG,"zaznamenal jsem chuzy v ruce");
								kroky ++;
								distance += DELKA_KROKU;
						
							}
						
						
						
					}
					
					
					
					pomcas = System.currentTimeMillis();
		
					
	    	
					
				}
				
				}
				// Check for other interesting types, if needed...
			 else
				Log.i(TAG,
						"Event does NOT contain native IContextInfo... we need to rely on the string representation!");
		}
		}

		@Override
		public void onContextRequestFailed(String requestId, String errorMessage, int errorCode) throws RemoteException {
			Log.w(TAG, "A1 - onContextRequestFailed for requestId " + requestId + " with error message: "
					+ errorMessage);
		}

		@Override
		public void onContextPluginDiscoveryStarted() throws RemoteException {
			Log.i(TAG, "A1 - onContextPluginDiscoveryStarted");
		}

		@Override
		public void onContextPluginDiscoveryFinished(List<ContextPluginInformation> discoveredPlugins)
				throws RemoteException {
			Log.i(TAG, "A1 - onContextPluginDiscoveryFinished");
		
			
		
		}

		@Override
		public void onDynamixFrameworkActive() throws RemoteException {
			Log.i(TAG, "A1 - onDynamixFrameworkActive");
		}

		@Override
		public void onDynamixFrameworkInactive() throws RemoteException {
			Log.i(TAG, "A1 - onDynamixFrameworkInactive");
			
		}

		@Override
		public void onContextPluginError(ContextPluginInformation plug, String message) throws RemoteException {
			Log.i(TAG, "A1 - onContextPluginError for " + plug + " with message " + message);
		}

		@Override
		public void onContextPluginEnabled(ContextPluginInformation plug) throws RemoteException {
			Log.i(TAG, "A1 - onContextPluginEnabled for " + plug);
		}

		@Override
		public void onContextPluginDisabled(ContextPluginInformation plug) throws RemoteException {
			Log.i(TAG, "A1 - onContextPluginDisabled for " + plug);
		
		}

		@Override
		public void onContextSupportResult(ContextSupportResult result) throws RemoteException {
			Log.i(TAG,
					"A1 - onContextSupportResult for id " + result.getResponseId() + " with success "
							+ result.wasSuccessful());
		}
	};

    
	/**
	 * Utility method that registers for the context types needed by this class. This method demonstrates the various
	 * ways of adding context support.
	 */
	private void registerForContextTypes() throws RemoteException {
		/*
		 * Method 1: Specifying a specific context type (without specifying a plug-in). In this case, the plug-in (or
		 * plug-ins) assigned to handle context type will be automatically selected by Dynamix.
		 */
		dynamix.addContextSupport(dynamixCallback, "org.ambientdynamix.contextplugins.ambientsound");
		dynamix.addContextSupport(dynamixCallback, "org.ambientdynamix.contextplugins.barcode");
		/*
		 * Method 2: Specifying a target plug-in and context type. In this case, Dynamix will only assign the specified
		 * plug-in to handle the context support.
		 */
	  dynamix.addPluginContextSupport(dynamixCallback, "org.ambientdynamix.contextplugins.nfc",
				"org.ambientdynamix.contextplugins.nfc.tag");
		/*
		 * Method 3: Specifying a target plug-in and a wildcard ("*") context type. In this case, Dynamix will
		 * automatically assign all of the plug-in's supported context types.
		 */
		dynamix.addPluginContextSupport(dynamixCallback, "org.ambientdynamix.contextplugins.sampleplugin",
				"*");
		dynamix.addPluginContextSupport(dynamixCallback, "org.ambientdynamix.contextplugins.pedometer", "org.ambientdynamix.contextplugins.pedometer");
		
		dynamix.addPluginContextSupport(dynamixCallback, "org.ambientdynamix.contextplugins.location", "org.ambientdynamix.contextplugins.location");
		
		
	}
	/**
	 * This metod show dialog for write comments about running and save data
	 */
	public void callDialog()
	{
		  alert = new AlertDialog.Builder(this);
	        alert.setTitle("Comment");
			alert.setMessage("Comment your running");

			// Set an EditText view to get user input 
			final EditText input = new EditText(this);
			alert.setView(input);

			alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Editable value = input.getText();
			  // Do something with value!
				
				koment = value.toString();
				
				if(koment.equals("")) koment = "No comment";
				
				expsave.saveXML(kroky, distance, speed, delkahod,
						delkamin, delkasec, listvzdalenosti, konec, zacatek, zemsirka, zemdelka, koment);

			  }
			});
			
			alert.show();
		
	}
	
	/**
	 * Write human height to file
	 * @param vyska2 - human height
	 */
	private void writeToFile(String vyska2) {
	    try {
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("configvyska.txt", Context.MODE_PRIVATE));
	        outputStreamWriter.write(vyska2);
	        outputStreamWriter.close();
	    }
	    catch (IOException e) {
	        Log.e("Exception", "File write failed: " + e.toString());
	    } 
	}
	
	/**
	 * Read human height from file
	 * @return string data from file
	 */
	private String readFromFile() {

	    String ret = "";

	    try {
	        InputStream inputStream = openFileInput("configvyska.txt");

	        if ( inputStream != null ) {
	            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
	            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	            String receiveString = "";
	            StringBuilder stringBuilder = new StringBuilder();

	            while ( (receiveString = bufferedReader.readLine()) != null ) {
	                stringBuilder.append(receiveString);
	            }

	            inputStream.close();
	            ret = stringBuilder.toString();
	        }
	    }
	    catch (FileNotFoundException e) {
	        Log.e("login activity", "File not found: " + e.toString());
	    } catch (IOException e) {
	        Log.e("login activity", "Can not read file: " + e.toString());
	    }

	    return ret;
	}
}
